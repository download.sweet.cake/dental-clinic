CREATE TABLE admin (
    id int NOT NULL AUTO_INCREMENT primary key,
    username text,
    user_password text
);
INSERT INTO admin (username, user_password)
VALUES
  ('Marina', '12345678'),
  ('Maria', '11111111'),
  ('Oleg', 'qwerty');