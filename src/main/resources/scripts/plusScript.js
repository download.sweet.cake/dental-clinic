function openModal(serviceType) {
    var modal = document.getElementById('myModal');
    var serviceTypeInput = modal.querySelector('#serviceType');

    serviceTypeInput.value = serviceType;

    modal.style.display = 'block';
}


function closeModal() {
    var modal = document.getElementById('myModal');
    modal.style.display = 'none';
}

function addNewService() {
    // Получите значения из полей ввода и добавьте новую услугу (ваша логика)
    // Закройте модальное окно после добавления
    closeModal();
}
