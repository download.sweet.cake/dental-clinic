CREATE TABLE appointment (
  appointment_id int NOT NULL AUTO_INCREMENT primary key,
  client_name TEXT,
  client_phone TEXT,
  appointment_date DATE,
  appointment_comment TEXT
);
INSERT INTO appointment (client_name, client_phone, appointment_date, appointment_comment) VALUES
('Мария', '+789828932748', DATE '2023-05-12', 'К пародонтологу на 3-6 часа');

