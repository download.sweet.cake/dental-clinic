package com.filippov.Dental.clinic.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DentalModel {
    private Long id;
    @NotNull
    @Size(min=3, message="Некорректное наименование!")
    private String serviceName;
    @NotNull
    private Integer servicePrice;
    private MedicalType medicalTypeService;
}
