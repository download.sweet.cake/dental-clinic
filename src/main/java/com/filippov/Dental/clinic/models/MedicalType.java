package com.filippov.Dental.clinic.models;

public enum MedicalType {
        CONSULTATION, THERAPY, SURGERY, PARODONTOLOGY, ORTHOPEDY, ORTHODONTICS, ENDODONTICS, HYGIENE, COSMETIC

}
