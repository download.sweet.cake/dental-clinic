package com.filippov.Dental.clinic.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DoctorModel {
    private int id;
    private String doctorName;
    private String doctorDescription;
    private String medicalTypeDoctor;
}
