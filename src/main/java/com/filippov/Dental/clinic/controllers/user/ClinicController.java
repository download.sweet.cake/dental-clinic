package com.filippov.Dental.clinic.controllers.user;

//import com.filippov.Dental.clinic.services.MedicalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class ClinicController {
    @GetMapping("/")
    public String clinic() {
        return "user/clinic";
    }

    @GetMapping("/contacts")
    public String contacts() {
        return "user/contacts";
    }
}