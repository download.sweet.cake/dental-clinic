package com.filippov.Dental.clinic.controllers.user;

import com.filippov.Dental.clinic.data.DentalRepository;
import com.filippov.Dental.clinic.models.DentalModel;
import com.filippov.Dental.clinic.models.MedicalType;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequiredArgsConstructor
public class MedServiceController {

    @Autowired
    private DentalRepository dentalRepository;

    @ModelAttribute
    public void addMedServicesToModel(Model model) {
        model.addAttribute("services", dentalRepository.allListDental());
        MedicalType[] medicalTypeServices = MedicalType.values();
        for (MedicalType medicalTypeService : medicalTypeServices) {
            model.addAttribute(medicalTypeService.toString().toUpperCase(),
                    dentalRepository.filterByType(medicalTypeService));
        }
    }


    @GetMapping("/services")
    public String showDesignForm(Model model) {
        return "user/services";
    }


    @GetMapping("/info/{service}")
    public String medicalServiceInfo(@PathVariable("service") String service, Model model) {
                model.addAttribute("services",
                        dentalRepository.filterByType(MedicalType.valueOf(service.toUpperCase())));
        return "user/aboutTypeService";
    }
    public Iterable<DentalModel> filterByType(
            List<DentalModel> medical, String typeService) {
        return medical
                .stream()
                .filter(x -> x.getMedicalTypeService().toString().toLowerCase().equals(typeService))
                .collect(Collectors.toList());
    }
}
