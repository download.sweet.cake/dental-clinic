package com.filippov.Dental.clinic.controllers.admin;

import com.filippov.Dental.clinic.data.DentalRepository;
import com.filippov.Dental.clinic.data.DoctorRepository;
import com.filippov.Dental.clinic.models.DentalModel;
import com.filippov.Dental.clinic.models.DoctorModel;
//import com.filippov.Dental.clinic.services.DoctorService;
//import com.filippov.Dental.clinic.services.MedicalService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminDoctorsController {

    @Autowired
    private DoctorRepository doctorRepository;


    @ModelAttribute
    public void addDoctorsToModel(Model model) {
        model.addAttribute("doctors", doctorRepository.listDoctors());
    }

    @GetMapping("/doctors")
    public String doctors() {
        return "admin/doctors";
    }
}
