package com.filippov.Dental.clinic.controllers.admin;

import com.filippov.Dental.clinic.data.AppointmentRepository;
import com.filippov.Dental.clinic.models.AppointmentModel;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/admin")
public class AdminAppointmentController {

    @Autowired
    private AppointmentRepository appointmentRepository;
    @GetMapping("/appointment")
    public String appointment(Model model) {
        model.addAttribute("appointments", appointmentRepository.allListAppointments());
        model.addAttribute("appointmentModel", new AppointmentModel());
        return "admin/appointment";
    }


//    @PostMapping("/appointment")
//    public String createAppointment(Model model, @Valid AppointmentModel appointmentModel, Errors errors, @RequestParam(value = "consent", required = false) String consent) {
//        if (!errors.hasErrors()&& "on".equals(consent)) {
//            appointmentRepository.save(appointmentModel);
//            return "redirect:/appointment";
//        }
//        return "/admin/appointment";
//    }
}
