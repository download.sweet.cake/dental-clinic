package com.filippov.Dental.clinic.controllers;

import com.filippov.Dental.clinic.data.AdminRepository;
import com.filippov.Dental.clinic.data.AppointmentRepository;
import com.filippov.Dental.clinic.models.AdminModel;
import com.filippov.Dental.clinic.models.AppointmentModel;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Controller
public class LoginController {

    @Autowired
    private AdminRepository adminRepository;

    @GetMapping("/login")
    public String login() {
        return "login";
    }
    @PostMapping("/login")
    public String login(Model model, AdminModel adminModel) {
        List<AdminModel> admins = (List<AdminModel>) adminRepository.allListAdmins();
        boolean isAdmin = false;
        for (AdminModel admin : admins) {
            if (admin.getUsername().equals(adminModel.getUsername()) && admin.getPassword().equals(adminModel.getPassword())) {
                isAdmin = true;
                break;
            }
        }
        if (isAdmin) {
            return "redirect:/admin/appointment";
        } else {
            return "redirect:/login";
        }
    }
}
