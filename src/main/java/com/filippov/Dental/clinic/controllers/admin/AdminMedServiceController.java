package com.filippov.Dental.clinic.controllers.admin;

import com.filippov.Dental.clinic.data.DentalRepository;
import com.filippov.Dental.clinic.models.AppointmentModel;
import com.filippov.Dental.clinic.models.DentalModel;
import com.filippov.Dental.clinic.models.MedicalType;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminMedServiceController {

    @Autowired
    private DentalRepository dentalRepository;

    @ModelAttribute
    public void addMedServicesToModel(Model model) {
        model.addAttribute("services", dentalRepository.allListDental());
        MedicalType[] medicalTypeServices = MedicalType.values();
        for (MedicalType medicalTypeService : medicalTypeServices) {
            model.addAttribute(medicalTypeService.toString().toUpperCase(),
                    dentalRepository.filterByType(medicalTypeService));
        }
    }

    @GetMapping("/openServiceModal")
    public String openServiceModal(Model model) {
        return "admin/services";
    }

//    @PostMapping("/add-service")
//    public String createService(Model model,
//                                @RequestParam("serviceName") String serviceName,
//                                @RequestParam("servicePrice") String servicePrice,
//                                @RequestParam("serviceType") String serviceType) {
//        try {
//            DentalModel dentalModel = new DentalModel();
//            dentalModel.setServiceName(serviceName);
//            dentalModel.setServicePrice(Integer.valueOf(servicePrice));
//            dentalModel.setMedicalTypeService(MedicalType.valueOf(serviceType));
//
//            dentalRepository.save(dentalModel);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "redirect:/admin/services";
//    }





    @GetMapping("/services")
    public String showDesignForm(Model model) {
        return "admin/services";
    }


    public Iterable<DentalModel> filterByType(
            List<DentalModel> medical, String typeService) {
        return medical
                .stream()
                .filter(x -> x.getMedicalTypeService().toString().toLowerCase().equals(typeService))
                .collect(Collectors.toList());
    }
}
