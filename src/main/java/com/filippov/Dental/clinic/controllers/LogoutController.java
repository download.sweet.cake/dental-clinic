package com.filippov.Dental.clinic.controllers;

import com.filippov.Dental.clinic.data.AdminRepository;
import com.filippov.Dental.clinic.data.AppointmentRepository;
import com.filippov.Dental.clinic.models.AdminModel;
import com.filippov.Dental.clinic.models.AppointmentModel;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Controller
public class LogoutController {

    @Autowired
    private AdminRepository adminRepository;

    @GetMapping("/logout")
    public String logout() {
        return "logout";
    }
    @PostMapping("/logout")
    public String logout(@RequestParam("confirm") boolean confirm) {
        if (confirm) {
            return "redirect:/";
        } else {
            return "redirect:/admin/appointment";
        }
    }
}
