package com.filippov.Dental.clinic.data;

import com.filippov.Dental.clinic.models.AppointmentModel;

import java.util.Optional;

public interface AppointmentRepository {
    Iterable<AppointmentModel> allListAppointments();
    Optional<AppointmentModel> findById(Long id);
    void save(AppointmentModel appointmentModel);
    void delete(Long id);

}
