package com.filippov.Dental.clinic.data;

import com.filippov.Dental.clinic.models.DoctorModel;
import com.filippov.Dental.clinic.models.MedicalType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcDoctorRepository implements DoctorRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcDoctorRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Iterable<DoctorModel> listDoctors() {
        return jdbcTemplate.query(
                "SELECT doctor_id, doctor_name, doctor_description, doctor_type FROM doctor",
                this::mapRowToDoctor);
    }

    public Optional<DoctorModel> findById(Long id) {
        List<DoctorModel> results = jdbcTemplate.query(
                "SELECT doctor_id, doctor_name, doctor_description, doctor_type FROM doctor WHERE doctor_id = ?",
                this::mapRowToDoctor,
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    public Optional<DoctorModel> filterByType(MedicalType medicalType) {
        List<DoctorModel> results = jdbcTemplate.query(
                "SELECT doctor_id, doctor_name, doctor_description, doctor_type FROM doctor WHERE ? = ANY (doctor_type)",
                this::mapRowToDoctor,
                medicalType.toString());
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    public DoctorModel save(DoctorModel doctorModel) {
        jdbcTemplate.update(
                "INSERT INTO doctor (doctor_name, doctor_description, doctor_type) VALUES (?, ?, ?)",
                doctorModel.getDoctorName(),
                doctorModel.getDoctorDescription(),
                doctorModel.getMedicalTypeDoctor()
                );
        return doctorModel;
    }

    private DoctorModel mapRowToDoctor(ResultSet row, int rowNum) throws SQLException {
        return new DoctorModel(
                row.getInt("doctor_id"),
                row.getString("doctor_name"),
                row.getString("doctor_description"),
                row.getString("doctor_type")
        );
    }

}
