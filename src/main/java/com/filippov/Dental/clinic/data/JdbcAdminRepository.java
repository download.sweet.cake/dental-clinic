package com.filippov.Dental.clinic.data;
import com.filippov.Dental.clinic.models.AdminModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcAdminRepository implements AdminRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcAdminRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public Iterable<AdminModel> allListAdmins() {
        return jdbcTemplate.query(
                "SELECT id, username, user_password FROM admin",
                this::mapRowToAdmin);
    }

    @Override
    public boolean findByPassword(String password) {
        List<AdminModel> results = jdbcTemplate.query(
                "SELECT id, username, user_password FROM admin WHERE user_password = ?",
                this::mapRowToAdmin,
                password);
        return !results.isEmpty();
    }

    @Override
    public boolean findByUserName(String username) {
        List<AdminModel> results = jdbcTemplate.query(
                "SELECT id, username, user_password FROM admin WHERE username = ?",
                this::mapRowToAdmin,
                username);
        return !results.isEmpty();
    }

    @Override
    public void save(AdminModel adminModel) {
        jdbcTemplate.update(
                "INSERT INTO admin (username, user_password) " +
                        "VALUES (?, ?)",
                adminModel.getUsername(),
                adminModel.getPassword());
    }

    public void delete(Long id) {
        String sql = "DELETE FROM appointment WHERE appointment_id = ?";
        jdbcTemplate.update(sql, id);
    }


    private AdminModel mapRowToAdmin(ResultSet row, int rowNum) throws SQLException {
        return new AdminModel(
                row.getLong("id"),
                row.getString("username"),
                row.getString("user_password"));
    }
}
