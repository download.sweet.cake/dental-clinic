package com.filippov.Dental.clinic.data;
import com.filippov.Dental.clinic.models.DentalModel;
import com.filippov.Dental.clinic.models.MedicalType;

import java.util.Optional;
public interface DentalRepository {
        Iterable<DentalModel> allListDental();
        Optional<DentalModel> findById(Long id);
        Iterable<DentalModel> filterByType(MedicalType medicalType);
        DentalModel save(DentalModel dentalModel);

    DentalModel update(DentalModel dentalModel);
}
