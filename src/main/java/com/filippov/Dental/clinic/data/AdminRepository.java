package com.filippov.Dental.clinic.data;

import com.filippov.Dental.clinic.models.AdminModel;

import java.util.Optional;

public interface AdminRepository {
    Iterable<AdminModel> allListAdmins();
    boolean findByPassword(String password);
    boolean findByUserName(String username);
    void save(AdminModel adminModel);
    void delete(Long id);
}
