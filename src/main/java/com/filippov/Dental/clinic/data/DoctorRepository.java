package com.filippov.Dental.clinic.data;

import com.filippov.Dental.clinic.models.DoctorModel;
import com.filippov.Dental.clinic.models.MedicalType;

import java.util.Optional;

public interface DoctorRepository {
    Iterable<DoctorModel> listDoctors();
    Optional<DoctorModel> findById(Long id);
    Optional<DoctorModel> filterByType(MedicalType medicalType);
    DoctorModel save(DoctorModel doctorModel);
}
