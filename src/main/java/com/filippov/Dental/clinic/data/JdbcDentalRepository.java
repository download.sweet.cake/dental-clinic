package com.filippov.Dental.clinic.data;
import com.filippov.Dental.clinic.models.DentalModel;
import com.filippov.Dental.clinic.models.MedicalType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcDentalRepository implements DentalRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcDentalRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<DentalModel> allListDental() {
        return jdbcTemplate.query(
                "select service_id, service_name, service_price, service_type from dental",
                this::mapRowToDental);
    }
    @Override
    public Optional<DentalModel> findById(Long id) {
        List<DentalModel> results = jdbcTemplate.query(
                "select service_id, service_name, service_price, service_type FROM dental WHERE service_id=?",
                this::mapRowToDental,
                id);
        return results.size() == 0 ?
                Optional.empty() :
                Optional.of(results.get(0));
    }

    public Iterable<DentalModel> filterByType(MedicalType medicalType) {
        return jdbcTemplate.query(
                "select service_id, service_name, service_price, service_type from dental WHERE service_type = ?",
                this::mapRowToDental, medicalType.toString());
    }

    @Override
    public DentalModel save(DentalModel dentalModel) {
        jdbcTemplate.update(
                "insert into dental (service_name, service_price, service_type) values (?, ?, ?)",
                dentalModel.getServiceName(),
                dentalModel.getServicePrice(),
                dentalModel.getMedicalTypeService().toString());
        return dentalModel;
    }
    @Override
    public DentalModel update(DentalModel dentalModel) {
        jdbcTemplate.update(
                "update into dental (service_name, service_price, service_type) values (?, ?, ?)",
                dentalModel.getServiceName(),
                dentalModel.getServicePrice(),
                dentalModel.getMedicalTypeService().toString());
        return dentalModel;
    }

    private DentalModel mapRowToDental(ResultSet row, int rowNum)
            throws SQLException {
        return new DentalModel(
                row.getLong("service_id"),
                row.getString("service_name"),
                row.getInt("service_price"),
                MedicalType.valueOf(row.getString("service_type")));
    }
}
