package com.filippov.Dental.clinic.data;
import com.filippov.Dental.clinic.models.AppointmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcAppointmentRepository implements AppointmentRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcAppointmentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<AppointmentModel> allListAppointments() {
        return jdbcTemplate.query(
                "SELECT appointment_id, client_name, client_phone, appointment_date, appointment_comment FROM appointment",
                this::mapRowToAppointment);
    }

    @Override
    public Optional<AppointmentModel> findById(Long id) {
        List<AppointmentModel> results = jdbcTemplate.query(
                "SELECT appointment_id, client_name, client_phone, appointment_date, appointment_comment FROM appointment WHERE appointment_id = ?",
                this::mapRowToAppointment,
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public void save(AppointmentModel appointmentModel) {
        jdbcTemplate.update(
                "INSERT INTO appointment (client_name, client_phone, appointment_date, appointment_comment) " +
                        "VALUES (?, ?, ?, ?)",
                appointmentModel.getClientName(),
                appointmentModel.getClientPhone(),
                appointmentModel.getAppointmentDate(),
                appointmentModel.getAppointmentComment());
    }

    public void delete(Long id) {
        String sql = "DELETE FROM appointment WHERE appointment_id = ?";
        jdbcTemplate.update(sql, id);
    }

    private AppointmentModel mapRowToAppointment(ResultSet row, int rowNum) throws SQLException {
        return new AppointmentModel(
                row.getInt("appointment_id"),
                row.getString("client_name"),
                row.getString("client_phone"),
                row.getDate("appointment_date"),
                row.getString("appointment_comment"));
    }
}
